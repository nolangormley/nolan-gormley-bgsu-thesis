from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import KFold
from timeit import default_timer as timer
from scipy.optimize import minimize
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

class LinReg:
    def __init__(self, x, y):
        self.x = np.array(x)
        self.variable = np.ones((self.x.shape[0], 2))
        self.variable[:, 0] = self.x
        self.target = np.array(y)

    def predict(self, m):
        return np.dot(self.variable, m)
        
    def SquaredError(self, m):
        sse = sum((self.predict(m) - self.target)**2)
        if self.verbose:
            print(f'\rSSE: {sse}', end = '\r')
        return sse

    def fit(self, folds=6, intial_coef=None, optim='Nelder-Mead', tolerance=None, verbose=False):
        self.verbose = verbose
        self.results = minimize(self.SquaredError, [.1, .1], method=optim, tol=tolerance)
        self.sse     = self.SquaredError(self.results.x)
        return self

    def fit_cv(self, folds=6, initial_coef=None, optim='Nelder-Mead', tolerance=None, verbose=False):
        self.verbose = verbose

        if initial_coef is None:
            initial_coef = list((np.random.rand(1, 2)*2-1)[0])

        variable = self.variable
        target   = self.target

        results  = []
        bestResult = None
        kf = KFold(n_splits=folds)
        kf.get_n_splits(self.variable)

        for train_index, test_index in kf.split(self.variable):
            self.variable = variable[train_index]
            self.target   = target[train_index]

            self.results = minimize(self.SquaredError, initial_coef, method=optim, tol=tolerance)

            modelResult = {
                'sse'      : self.SquaredError(self.results.x),
                'results'  : self.results,
            }

            if bestResult is None or modelResult['sse'] < bestResult['sse']:
                bestResult = modelResult

            results.append(modelResult)
        
        self.variable    = variable
        self.target      = target
        self.results     = bestResult['results']
        self.sse         = self.SquaredError(self.results.x)
        self.cvresults   = results

        return self

if __name__ == "__main__":
    df = pd.read_csv('https://storage.googleapis.com/natl-iso-map.appspot.com/pw_data3_500.csv')
    ss  = MinMaxScaler()
    foo = ss.fit_transform(df)
    foo = pd.DataFrame(foo)

    start = timer()

    model = LinReg(foo[0], foo[1])
    results = model.fit()
    print(results)

    end = timer()

    print('\n\nTime:', end - start)

    start = timer()
    linModel = LinearRegression(n_jobs=-1).fit(np.array(foo[0]).reshape(-1,1), np.array(foo[1]))
    print(linModel.coef_)
    print(linModel.intercept_)
    end = timer()

    print('\n\nTime:', end - start)
    
    plt.title('Minimize SSE Example')
    plt.scatter(foo[0], foo[1])
    plt.plot(foo[0], model.predict(results.x), 'r*')
    plt.plot(foo[0], model.predict(np.array([linModel.coef_[0], linModel.intercept_])), 'b')
    plt.show()

    print(model.SquaredError(results.x))
    print(model.SquaredError(np.array([linModel.coef_[0], linModel.intercept_])))   