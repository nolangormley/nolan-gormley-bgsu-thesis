import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons

x = np.linspace(-1, 1, 500)
fig, ax = plt.subplots()
plt.subplots_adjust(bottom=0.25)
alpha = .01

def SofterMax(alpha, x):
    if x > 0 and x <= alpha:
        return round(3 * alpha**(-4) * x**5 - 8 * alpha**(-3) * x**4 + 6 * alpha**(-2) * x**3, 8)
    elif x > alpha:
        return x
    else:
        return 0

def SofterMaxPrime(alpha, x):
    if x > 0 and x <= alpha:
        return(round(15 * alpha**(-4) * x**4 - 32 * alpha**(-3) * x**3 + 18 * alpha**(-2) * x**2 , 8))
    elif x > alpha:
        return 1
    else:
        return 0

def SofterMaxPrimePrime(alpha, x):
    if x > 0 and x <= alpha:
        return(round(60 * alpha**(-4) * x**3 - 96 * alpha**(-3) * x**2 + 36 * alpha**(-2) * x, 8))
    else:
        return 0

softerMax = []
softerMaxPrime = []
softerMaxPrimePrime = []
for val in x:
    softerMax.append(SofterMax(alpha, val))
    softerMaxPrime.append(SofterMaxPrime(alpha, val))
    softerMaxPrimePrime.append(SofterMaxPrimePrime(alpha, val))

l, = plt.plot(x, softerMax)
m, = plt.plot(x, softerMaxPrime)
n, = plt.plot(x, softerMaxPrimePrime)
plt.legend(['SofterMax', '1st Der. SofterMax', '2nd Der. SofterMax'])

ax.margins(x=0)
axalpha = plt.axes([0.18, 0.1, 0.65, 0.03])

salpha = Slider(axalpha, 'Alpha', 0, 1, valinit=alpha)

def update(val):
    alpha = salpha.val
    softerMax = []
    softerMaxPrime = []
    softerMaxPrimePrime = []
    for val in x:
        softerMax.append(SofterMax(alpha, val))
        softerMaxPrime.append(SofterMaxPrime(alpha, val))
        softerMaxPrimePrime.append(SofterMaxPrimePrime(alpha, val))
        
    l.set_ydata(softerMax)
    m.set_ydata(softerMaxPrime)
    n.set_ydata(softerMaxPrimePrime)
    fig.canvas.draw_idle()


salpha.on_changed(update)

ax.set_ylim((-5,5))

ax.set_title('SofterMax')
fig.canvas.set_window_title('SofterMax Alpha Visualization')
plt.show()