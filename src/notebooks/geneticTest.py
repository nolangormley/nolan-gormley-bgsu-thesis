# Quick work around to import packages in the parent directory
import sys
sys.path.insert(0,'..')

from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import KFold
from timeit import default_timer as timer
from sklearn.utils import resample
import matplotlib.pyplot as plt
from matrixMath import Knotilus
import pandas as pd
import numpy as np

def geneticComp_cv(data, folds, iterations):
    fullVariable = np.array(data[0])
    fullTarget   = np.array(data[1])

    for i in range(iterations):
        kf = KFold(n_splits=folds, shuffle=True)
        kf.get_n_splits(fullVariable)

        results = []
        bestResults = {}
        methods = ('Nelder-Mead', 'SLSQP', 'pso', 'diffev')
        fold = 0
        for train_index, test_index in kf.split(fullVariable):
            variable = fullVariable[train_index]
            target   = fullTarget[train_index]

            for method in methods:
                start = timer()

                model = Knotilus(variable, target)
                model = model.fit(optim=method, numKnots='auto', verbose=True)

                end = timer()

                model.variable = fullVariable[test_index]
                model.target   = fullTarget[test_index]

                test_sse = model.SquaredError(model.knotLoc)

                model.variable = fullVariable
                model.target   = fullTarget

                sse = model.SquaredError(model.knotLoc)

                result = {
                    'method' : method,
                    'test-sse' : test_sse,
                    'sse'  : sse,
                    'numKnots' : model.numKnots,
                    'knotLoc'  : model.knotLoc,
                    'iterations' : model.iterations,
                    'time' : end-start
                }

                results.append(result)

def geneticComp_boot(data, iters, split, verbose=False):
    fullVariable = np.array(data[0])
    fullTarget   = np.array(data[1])

    results = []
    bestResults = {}
    methods = ('Nelder-Mead', 'SLSQP', 'pso', 'diffev')
    for i in range(iters):
        if verbose:
            print('Iteration:', i)

        trainIndices = resample(np.arange(fullVariable.shape[0]), replace=True, n_samples=int(fullVariable.shape[0] * split))
        testIndices = np.array([ind for ind in np.arange(fullVariable.shape[0]) if ind not in trainIndices])
        X_train = fullVariable[trainIndices]
        y_train = fullTarget[trainIndices]
        X_test  = fullVariable[testIndices]
        y_test  = fullTarget[testIndices]

        for method in methods:
            if verbose:
                print('Running', method)
            start = timer()

            model = Knotilus(X_train, y_train)
            model = model.fit(optim=method, numKnots='auto', gamma=1e-4, alpha=1e-3, verbose=False)

            end = timer()

            train_rmse = model.RMSE(model.knotLoc)

            model.variable = X_test
            model.target   = y_test

            test_rmse = model.RMSE(model.knotLoc)

            result = {
                'iteration' : i,
                'method' : method,
                'test-rmse' : test_rmse,
                'train-rmse'  : train_rmse,
                'numKnots' : model.numKnots,
                'knotLoc'  : model.knotLoc,
                'iterations' : model.iterations,
                'time' : end-start
            }

            if method not in bestResults.keys() or result['test-rmse'] < bestResults[method]['test-rmse']:
                bestResults[method] = result

            results.append(result)

    return (bestResults, results)

df = pd.read_csv('../data/us_covid19_daily.csv')
df['deathIncrease'] = df['deathIncrease'].astype(int)
df['date'] = pd.to_datetime(df['date'], format='%Y%m%d')
df['unixTime'] = df['date'].astype(int) / 10**9
df = df[['unixTime', 'deathIncrease']]

ss  = MinMaxScaler()
foo = ss.fit_transform(df)
covid = pd.DataFrame(foo)

df = pd.read_csv('../data/Bike-Sharing-Dataset/day.csv')
df = df[['temp', 'cnt']]

ss  = MinMaxScaler()
foo = ss.fit_transform(df)
bike = pd.DataFrame(foo)

df = pd.read_csv('../data/pw_data3_1000.csv')

ss  = MinMaxScaler()
foo = ss.fit_transform(df)
gen1 = pd.DataFrame(foo)

df = pd.read_csv('../data/pw_data3_5000.csv')

ss  = MinMaxScaler()
foo = ss.fit_transform(df)
gen5 = pd.DataFrame(foo)

results = geneticComp_boot(covid, 100, .9)

pd.DataFrame.from_dict(results[0], orient='index').to_csv(f'../data/genetic_bestresultsboot_covid.csv')
pd.DataFrame.from_records(results[1]).to_csv(f'../data/genetic_resultsboot_covid.csv')

results = geneticComp_boot(bike, 100, .9)

pd.DataFrame.from_dict(results[0], orient='index').to_csv(f'../data/genetic_bestresultsboot_bike.csv')
pd.DataFrame.from_records(results[1]).to_csv(f'../data/genetic_resultsboot_bike.csv')

# results = geneticComp_boot(gen1, 100, .9)

# pd.DataFrame.from_dict(results[0], orient='index').to_csv(f'../data/genetic_bestresultsboot_gen1.csv')
# pd.DataFrame.from_records(results[1]).to_csv(f'../data/genetic_resultsboot_gen1.csv')

# results = geneticComp_boot(gen5, 100, .9)

# pd.DataFrame.from_dict(results[0], orient='index').to_csv(f'../data/genetic_bestresultsboot_gen5.csv')
# pd.DataFrame.from_records(results[1]).to_csv(f'../data/genetic_resultsboot_gen5.csv')