# Computational Experiments in Piecewise Regression Knot Selection
Nolan Gormley

## Introduction

This thesis is focused on implementing, comparing, and optimizing a novel approach to piecewise regression knot selection. There are many parts to this method that allow for the knots to be optimized on using out-of-box optimization methods. First and foremost, the representation of the knots is done by essentially binning the dataset for each knot. More on this in its respective section.

## Representation and SofterMax

The method of representing the knots within the dataset is a key concept in this method. It works as the following. Imagine you have a single variate dataset such as:

| Variable |
| ---- |
| 0 |
| .1 |
| .2 |
| .3 |
| .4 |
| .5 |
| .6 |
| .7 |
| .8 |
| .9 |

We want to transform this dataset with the knot values `[0, .2, .6]`, the dateset would then become:

| Knot 1 | Knot 2 | Knot 3 |
| ------ | ------ | ------ |
| 0 | 0 | 0 |
| .1 | 0 | 0 |
| .2 | 0 | 0 |
| .3 | .1 | 0 |
| .4 | .2 | 0 |
| .5 | .3 | 0 |
| .6 | .4 | 0 |
| .7 | .5 | .1 |
| .8 | .6 | .2 |
| .9 | .7 | .3 |

Using this representation, we can cast the problem of piecewise regression as a multi-variate regression problem.

The equation we use to calculate this representation is as follows:

```
augmentedData = np.array(shape=(knots.shape[0], data.shape[0]))
for i in range(len(knots)):
    augmentedData[i] = SofterMax(data - knot)
```

Note the use of `SofterMax`. This is a polynomial representation of a max function. Given an alpha value of around 1e-8, it behaves very similarly to a max function. By incresing this value, we can start to 'leak' across the knots near the edges. This allows the model to become polynomial rather than having harsh breaks at the changes in functions. This can mitigate problems with multiple knots being places in small locations causing overfitting.

## Optimization Routines

I will compare two different families of optimization routines. These are heuristic and stochastic methods. The heuristic methods, in this case, are methods of optimization that take advantage of the estimated gradient in order to decide how the method will change. Stochastic methods differ from this by adding a random aspect to their decision making. In theory, this randomness will stop the method from getting stuck in local minima and find the global best solution more often and with more stability.

Here is an example of two knot selection optimization methods. These plots show you the optimizations results per iteration with the final state being the final result that the method returned.

![Nelder Mead Optimization](https://gormley.dev/static/Nelder-Mead_animation.mp4)
![Differential Evolution Optimization](https://gormley.dev/static/diffev_animation.mp4)